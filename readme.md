# Counting number of peoples with mask and without mask

Python implementation for counting number of people who are wearing mask and not wearing mask

## Requirements
* Tested on Python 3.6.10
*       pip install -r requirements.txt

## Usage 

        python data_preparation.py
        python main.py
