import os
import cv2
import numpy as np

# Give the path for input image database
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# To
image_dir = os.path.join(BASE_DIR,"Images\without_mask")
data = []
haar_data = cv2.CascadeClassifier('data.xml')

# To read all image file from database and to store employee id wrt

for root,dirs,files in os.walk(image_dir):
    for file in files:
            if file.endswith("jpeg") or file.endswith("jpg"):
                image = cv2.imread(os.path.join(root,file))
                #print(image.shape)
                faces = haar_data.detectMultiScale(image)
                for x, y, w, h in faces:
                    cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)
                    face = image[y:y + h, x:x + w, :]
                    face = cv2.resize(face, (50, 50))
                    data.append(face)
                np.save('without_mask.npy',data)

