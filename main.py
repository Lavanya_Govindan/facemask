import cv2
import numpy as np
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split

class face_test:

    with_mask = np.load('with_mask.npy')
    without_mask = np.load('without_mask.npy')

    n1,x1,y1,z1 = with_mask.shape
    n2,x2,y2,z2 = without_mask.shape
    with_mask = with_mask.reshape(n1,x1*y1*z1)
    without_mask = without_mask.reshape(n2,x2*y2*z2)

    X = np.r_[with_mask,without_mask]

    labels = np.zeros(X.shape[0])
    labels[n1:] = 1.0

    x_train,x_test,y_train,y_test = train_test_split(X,labels,test_size=0.20)

    pca = PCA(n_components=3)
    x_train = pca.fit_transform(x_train)
    x_test = pca.transform(x_test)

    clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(8, 3), random_state=1)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)

    print(accuracy_score(y_test,y_pred))

    # Face Recognition
    haar_data = cv2.CascadeClassifier('data.xml')

    # Capture live video
    cap = cv2.VideoCapture("task_video.mp4")
    font = cv2.FONT_HERSHEY_SIMPLEX
    #fourcc = cv2.VideoWriter_fourcc(*'XDIV')
    #out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))
    while(True):
        ret, frame = cap.read()
        faces = haar_data.detectMultiScale(frame)
        mask_num = 0
        no_mask_num = 0

        # Segment the face
        for x,y,w,h in faces:
            cv2.rectangle(frame, (x, y), (x+w,y+h), (255,0,0),2)
            face = frame[y:y+h,x:x+w,:]
            face = cv2.resize(face,(50,50))
            face = face.reshape(1,-1)
            face = pca.transform(face)
            pred = clf.predict(face)
            if int(pred) == 0:
                mask_num = mask_num+1
            else:
                no_mask_num = no_mask_num+1
            cv2.putText(frame,"Mask:" + str(mask_num),(20,20),font,0.7,(0,250,0),1)
            cv2.putText(frame,"No Mask:"+ str(no_mask_num),(20,40),font,0.7,(0,0,250),1)
        # Display the resulting frame
        cv2.imshow('frame',frame)
        #out.write(frame)

        if cv2.waitKey(20) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()